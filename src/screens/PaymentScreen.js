import React, { useState } from "react";
import '../assets/css/payment.css';
import Visa from '../assets/image/visa.png';
import Credit from '../assets/image/credit-card.png';
import Master from '../assets/image/maestro.png';

import Header  from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default function PaymentScreen() {
    const [biaya, setBiaya] = useState("");
    const handler = e => {
        setBiaya(e.target.value)
    }
    return(
    <>
        <div class="row">
        <Footer/>
        <div class="col-75">
            <div class="container">
            <form action="#">
            <div class="row">
                
                <div class="col-50">
                    <h3>Payment</h3>
                    <label for="fname">Accepted Cards</label>
                    <div class="icon-container">
                        <img src={Visa}></img>
                        <img src={Credit}></img> 
                        <img src={Master}></img> 
                    </div>
                    <label for="cname">Name on Card</label>
                    <input type="text" id="cname" name="cardname" placeholder="nama"/>
                    <label for="ccnum">Credit card number</label>
                    <input type="text" id="ccnum" name="cardnumber" placeholder="xxxx-xxxx-xxxx-xxxx"/>
                    <label for="expmonth">Exp Month</label>
                    <input type="text" id="expmonth" name="expmonth" placeholder="xx/xx"/>
                    <div class="row">
                    <div class="col-50">
                        <label for="expyear">Exp Year</label>
                        <input type="text" id="expyear" name="expyear" placeholder="tahun"/>
                    </div>
                    <div class="col-50">
                        <label for="cvv">CVV</label>
                        <input type="text" id="cvv" name="cvv" placeholder="xxx"/>
                    </div>
                    </div>
                </div>
                </div>
                <input type="submit" value="Lanjutkan Pembayaran" class="btn"/>
            </form>
            </div>
        </div>
        <div class="col-25">
            <form action="#"> 
                <div class="col-50">
                    <h2>Biaya per Adopsi Rp 30.000,-</h2>
                    <label for="jumlah-adopsi">Insert Amount</label>
                    <input type="text" id="jumlah-adopsi" name="jumlah-adopsi" value={biaya} placeholder="1,2,3,4...." onChange={handler}/>
                    <label for="total-bayar">Total Payment</label>
                    <h3>Rp. {biaya}</h3>
                </div>
            </form>
        </div>
        <Header/>
        </div>  
    </>
    );
}