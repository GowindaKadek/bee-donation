import React from 'react';
import { CssBaseline, Grid } from '@material-ui/core';
import { Link } from 'react-router-dom';
import '../assets/css/home.css';
import Adopter from '../assets/image/15.jpg';

import Header  from '../components/Header/Header';
import Footer from '../components/Footer/Footer';

export default function HomeScreen() {

  return(
    <>
      <CssBaseline/>
      <Header/>
        <div class='content'>
            <div class="textBox">
                <h2><br/> Website <span> Adopsi </span></h2><br/>
                <p><span>Website Adopsi</span> ini dikembangkan oleh yayasan Bali Trensa Sujati dalam rangka memberikan kesempatan pada mereka yang ingin berpartisipasi dalam pelesatrian alam.
                </p>
                <Link to="/products" href>Coba Sekarang</Link>
            </div>
            <div class="imgBox">
                <img src={Adopter} class="Adopsi-img"/>
            </div>
        </div>
      <Footer/>
    </>
  )
}