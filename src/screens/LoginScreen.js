import React from "react";
import './../assets/css/login.css';
import { Link } from "react-router-dom";

import icon1 from './../assets/image/icon1.png';
import icon2 from './../assets/image/icon2.png';
import logo from './../assets/image/logo.png';

export default function LoginScreen(setIsLogin) {
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [isPasswordHide, setIsPasswordHide] = React.useState(true);

    const passtype = (hide) => {
        if(hide){
            return 'password'
        }else return 'text'
    }
    
    return (
        <div id="login-screen">
            <Link to="/" className="brand-title">
                <h1>Bee-Donation</h1>
                <img src = {logo} className="logo"/>
            </Link>
            <div className="box-container">
                <div className="box-header-container">
                    <h3>Masuk</h3>
                    <Link to="/register" className="register-button">
                        Daftar
                    </Link>
                </div>
                <form className="login-form" onSubmit="">
                    <label>Email</label>
                    <input
                        type="email"
                        required={true}
                        name="email"
                        value={email}
                        onChange=""
                        placeholder="Masukkan email"/>
                    <label>Password</label>
                    <div className="password-input" >
                        <input
                            type= {passtype(isPasswordHide)}
                            required={true}
                            name="password"
                            value={password}
                            placeholder="Masukkan password"/>
                        {isPasswordHide
                            ? <img src={icon1} className="hide-see" onClick={() => setIsPasswordHide((prev) => !prev)}/>
                            : <img src={icon2} className="hide-see" onClick={() => setIsPasswordHide((prev) => !prev)}/>
                        }
                    </div>
                    <div className = "submit">
                        <button className = "submit-button">
                            Login
                        </button>
                    </div>
                </form>
                <div className="fb-container">
                    <text>atau masuk menggunakan</text>
                    
                </div>
            </div>
        </div>
    )
}