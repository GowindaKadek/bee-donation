import React from 'react';
import './../assets/css/register.css';
import {Link, useHistory} from 'react-router-dom';
import icon1 from './../assets/image/icon1.png';
import icon2 from './../assets/image/icon2.png';
import logo from './../assets/image/logo.png';

export default function RegisterScreen(setIsLogin) {
    const [name, setName] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [confirmPassword, setConfirmPassword] = React.useState("");
    const [isPasswordHide, setIsPasswordHide] = React.useState(true)
    const [isPasswordHideCf, setIsPasswordHideCf] = React.useState(true)

    const passtype = (hide) => {
        if(hide){
            return 'password'
        }else return 'text'
    }

    return (
        <div id="register-screen">
            <Link to="/" className="brand-title">
                <h1>Bee-Donation</h1>
                <img src = {logo} className="logo"/>
            </Link>
            <div className="box-container">
                <div className="box-header-container">
                    <h3>Daftar</h3>
                    <Link to="/login" className="login-button">
                        Masuk
                    </Link>
                </div>
                <form className="register-form" onSubmit="">
                    <label>Nama Lengkap</label>
                    <input
                        type="text"
                        required={true}
                        name="name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        placeholder="Masukkan nama lengkap"/>
                    <label>Email</label>
                    <input
                        type="email"
                        required={true}
                        name="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Masukkan email"/>
                    <label>Password</label>
                    <div className="password-input" >
                        <input
                            type={passtype(isPasswordHide)}
                            required={true}
                            name="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            placeholder="Masukkan password"/>
                        {isPasswordHide
                            ? <img src={icon1} className="hide-see" onClick={() => setIsPasswordHide((prev) => !prev)}/>
                            : <img src={icon2} className="hide-see" onClick={() => setIsPasswordHide((prev) => !prev)}/>
                        }
                    </div>
                    <label>Konfirmasi Password</label>
                    <div className="password-input" >
                        <input
                            type={passtype(isPasswordHideCf)}
                            required={true}
                            name="confirm_password"
                            value={confirmPassword}
                            onChange={(e) => setConfirmPassword(e.target.value)}
                            placeholder="Konfirmasi password"/>
                        {isPasswordHideCf
                            ? <img src={icon1} className="hide-see" onClick={() => setIsPasswordHideCf((prev) => !prev)}/>
                            : <img src={icon2} className="hide-see" onClick={() => setIsPasswordHideCf((prev) => !prev)}/>
                        }
                    </div>
                    <div className = "submit">
                        <button className = "submit-button">
                            Daftar
                        </button>
                    </div>
                </form>
                <div className="fb-container">
                    <p>atau daftar menggunakan</p>
                    
                </div>
            </div>
        </div>
    )
    
}