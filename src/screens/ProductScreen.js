import React, { useState, useEffect, lazy, Suspense } from 'react';
import { CssBaseline, Grid } from '@material-ui/core';

import {getPlacesData} from '../api';

import Header  from '../components/Header/Header';
import List from '../components/List/List';
const Maps = lazy (() =>import ('../components/Maps/Maps'));

export default function ProductScreen() {
  const [places, setPlaces] = useState([]);
  const [childClicked, setChildClicked] = useState(null);
  const [displayIt, setDisplayIt] = useState(false);

  const [coordinates, setCoordinates] = useState({lat:-8.509041876415104, lng: 115.38048790955044});
  const [bounds, setBounds] = useState({});

  useEffect (() => {
    setTimeout (() => 
      setDisplayIt(prev => (!prev)),2000)
  },[]);
  console.log(displayIt);

  useEffect(() => {
    getPlacesData()
      .then((data) => {
        
        setPlaces(data);
      })
  }, [coordinates, bounds]);

  return(
    <>
      <CssBaseline/>
      <Header/>
      <Grid container spacing= {3} style = {{width: "100%", paddingTop: "30px"}}>
        <Grid item xs = {12} md={4}>
          <List 
            places = {places}
            childClicked = {childClicked}
          />
        </Grid>
        <Grid item xs = {12} md={8}>
          <Suspense fallback = {<p>Loading Maps...</p>}>
          {displayIt && 
          <Maps 
              setCoordinates = {setCoordinates}
              setBounds = {setBounds}
              coordinates = {coordinates}
              places = {places}
              setChildClicked = {setChildClicked}
            />}
          </Suspense>
        </Grid>
      </Grid>
    </>
  )
}