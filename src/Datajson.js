const peternakan = [
    {
        "id" : 1,
        "name" : "Peternakan Pak Wayan",
        "email" : "WayanKella@gmail.com",
        "phone" : "081933900401",
        "lat" : -8.507931453822449, 
        "lng" : 115.38036794951178,
        "address" : "Br. Sibang kaja, Desa Sibang, Kecamatan mambar, Kabupaten Badung, Bali",
        "info" : "Pembersihan kandang mingguan",
        "img" : require('./assets/image/1.jpg')
    },
    {
        "id" : 2,
        "name" : "Peternakan bee-kella",
        "email" : "kellakella479@gmail.com",
        "phone" : "082247865309",
        "lat" : -8.509405814943278, 
        "lng" : 115.38028751231415,
        "address" : "Br. Umahanyar, Desa Pejeng Kaja, Kacamatan Tampaksiring, Kabupaten Gianyar, Bali ",
        "info" : "Pengawinan lebah kella",
        "img" : require('./assets/image/2.jpg')
    },
    {
        "id" : 3,
        "name" : "Budidaya Lebah Kella",
        "email" : "Budidayakella89@gmail.com",
        "phone" : "089765345234",
        "lat" : -8.51130413786605, 
        "lng" : 115.38058157486385,
        "address" : "Br. Teges, Desa Peliatan, Kecamatan Ubud, Kabupaten Gianyar, Provinsi Bali",
        "info" : "Pengiriman sarang lebah ke distributor",
        "img" : require('./assets/image/3.jpg')
    },
    {
        "id" : 4,
        "name" : "Bee Kella Amigos",
        "email" : "AMigoskella2002@gmail.com",
        "phone" : "081933001290",
        "lat" : -8.509251058942848, 
        "lng" : 115.38390058716179,
        "address" : "Br. Tulikup Kaja, Desa Tulikup, Kecamatan Gianyar, Kabupaten Gianyar, Bali",
        "info" : "Pembuatan sarang baru",
        "img" : require('./assets/image/4.jpg')
    },
    {
        "id" : 5,
        "name" : "Super Kella",
        "email" : "Kallasuperboom@gmail.com",
        "phone" : "082778673648",
        "lat" : -8.501851809989784, 
        "lng" : 115.3746866541105,
        "address" : "Br. Sanding Serongga, Desa Sanding, Kecamatan Tampaksiring, Kabupaten Gianyar, Bali",
        "info" : "Panen madu kella dalam kemasan botol",
        "img" : require('./assets/image/5.jpg')
    },
    {
        "id" : 6,
        "name" : "Peternakan Lebah Axios",
        "email" : "axioskella@gmail.com",
        "phone" : "083113567453",
        "lat" : -8.50819211114697, 
        "lng" : 115.37331390940562,
        "address" : "Br. Sembuwuk, Desa Pejeng Kaja, Kecamatan Tampaksiring, Kabupaten Gianyar, Bali",
        "info" : "pemasangan sarang baru",
        "img" : require('./assets/image/6.jpg')
    },
{
        "id" : 7,
        "name" : "Peternakan Lebah sukamaju",
        "email" : "Sukamajukella@gmail.com",
        "phone" : "081933254788",
        "lat" : -8.529838482228753,  
        "lng" : 115.36366518418599,
        "address" : "Br. Panglan, Desa Pejeng Kelod, Kecamatan Tampaksiring, Kabupaten Gianyar, Bali",
        "info" : "pemisahan madu dengan lebah kella",
        "img" : require('./assets/image/7.jpg')
    },
    {
        "id" : 8,
        "name" : "Kella bee factory",
        "email" : "WayanKella@gmail.com",
        "phone" : "082232767888",
        "lat" : -8.479358949493598, 
        "lng" : 115.38753457815879 ,
        "address" : "Br. Intaran, Desa Pejeng, Kecamatan Tampaksiring, Kabupaten Gianyar, Bali",
        "info" : "pemberian bibit baru pada sarang baru",
        "img" : require('./assets/image/8.jpg')
    },
    {
        "id" : 9,
        "name" : "Lebah Kella Production",
        "email" : "Kellaproduction2010@gmail.com",
        "phone" : "081234676453",
        "lat" : -8.492890458600383, 
        "lng" : 115.3655747886835,
        "address" : "Br. Laplapan, Desa Mas, Kecamatan Ubud, Kabupaten Gianyar, Provinsi Bali",
        "info" : "",
        "img" : require('./assets/image/9.jpg')
    },
    {
        "id" : 10,
        "name" : "Lebah Kella asique",
        "email" : "Kellasilebah@gmail.com",
        "phone" : "082237654997",
        "lat" : -8.52097770735989, 
        "lng" : 115.36193432028398, 
        "address" : "Br. Kubu Alit, Desa Kedonganan, Kecamatan Kuta, Kabupaten Badung, Bali.",
        "info" : "Membuat pamflet untuk bule",
        "img" : require('./assets/image/10.jpg')
    },
    {
        "id" : 11,
        "name" : "Madu asli madu kella",
        "email" : "inikellaasli@gmail.com",
        "phone" : "087654536888",
        "lat" : -8.485861525544399, 
        "lng" : 115.44601249802865,
        "address" : "Br. Ambengan, Desa Pedungan, Kecamatan Denpasar Selatan, Kabupaten Denpasar, Bali",
        "info" : "Mengecek sarang dan perbaikan sarang rusak",
        "img" : require('./assets/image/11.jpg')
    },
    {
        "id" : 12,
        "name" : "Pondok Lebah Kella asli",
        "email" : "pondokkellapunya@mail.com",
        "phone" : "087760008976",
        "lat" : -8.523713898044464,  
        "lng" : 115.36577701580929,
        "address" : "Br. Kekeran, Desa Penatahan, Kecamatan Penebel, Kabupaten Tabanan, Bali",
        "info" : "Menaruh bibit baru pada kandang lama",
        "img" : require('./assets/image/12.jpg')
    },
    {
        "id" : 13,
        "name" : "Peternakan asli kella-kella bee",
        "email" : "kellabeekella@gmail.com",
        "phone" : "082235676243",
        "lat" : -8.486317600537957, 
        "lng" : 115.35194331197562,
        "address" : "Br. Serokadan, Desa Abuan, Kecamatan Susut, Kabupaten Bangli, Bali",
        "info" : "Peletakan kandang baru kembali",
        "img" : require('./assets/image/13.jpg')
    },
    {
        "id" : 14,
        "name" : "Lebah Kella-Joon",
        "email" : "joonekella@gmail.com",
        "phone" : "081933564788",
        "lat" : -8.523713898044464,  
        "lng" : 115.4204969998625,
        "address" : "Br. Kapal, Desa Batubulan, Kecamatan Sukawati, Kabupaten Gianyar, Bali",
        "info" : "Panen kella dan peremajaan kandang",
        "img" : require('./assets/image/14.jpg')
    },
    {
         "id" : 15,
        "name" : "Lebah Kella Unggul",
        "email" : "Kellaunggul666@gmail.com",
        "phone" : "082234567765",
        "lat" : -8.54139765534836, 
        "lng" : 115.40510610897431,
        "address" : "Br. Bajing, Desa Tegak, Kecamatan Klungkung, Kabupaten Klungkung, Bali",
        "info" : "distribusi kella botol dan panen kella bulanan",
        "img" : require('./assets/image/15.jpg')     
    },
]
export default peternakan;