import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  chip: {
    margin: '5px 5px 5px 0',
  },
  proses:{
    width: "100%",
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    display: 'flex',
    textDecoration: 'none',
    alignItems: 'center',
    height: '30px',
    justifyContent: 'center',
    borderRadius: 5,
    backgroundColor: '#D8D3D3',
    width: '17%'
  },
  subtitle: {
    display: 'flex', alignItems: 'center', justifyContent: 'space-between', marginTop: '10px',
  },
  spacing: {
    display: 'flex', alignItems: 'center', justifyContent: 'space-between',
  },
  longText:{
    width : '70%',
    textAlign: 'right'
  },
}));