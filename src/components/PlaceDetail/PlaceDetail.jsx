import React from 'react';
import { Box, Typography, Button, Card, CardMedia, CardContent, CardActions, Chip } from '@material-ui/core';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import PhoneIcon from '@material-ui/icons/Phone';
import { Link } from 'react-router-dom';

import useStyles from './style';

const PlaceDetail = ({place}) => {
    var images = place.img;
    const classes = useStyles();
    return(
        <Card elevation={6}>
            <CardMedia 
                style={{height:350}}
                image = {images.default}
                title= {place.name}
            />
            <CardContent>
                <Typography gutterBottom variant="h5">{place.name}</Typography>
                <Box display="flex" justifyContent="space-between">
                    <Typography variant="subtitle1">Email</Typography>
                    <Typography gutterBottom variant="subtitle1">{place.email}</Typography>
                </Box>
                <Box display="flex" justifyContent="space-between">
                    <Typography variant="subtitle1">New*</Typography>
                    <Typography gutterBottom variant = "caption"><text className={classes.longText}>{place.info}</text></Typography>
                </Box>
                {place?.address && (
                    <Typography gutterBottom variant="caption" color="textSecondary" className={classes.subtitle}>
                        <LocationOnIcon /> 
                        <text className={classes.longText}>{place.address}</text>
                    </Typography>
                )}
                {place?.phone && (
                    <Typography gutterBottom variant="subtitle2" color="textSecondary" className={classes.spacing}>
                        <PhoneIcon /> {place.phone}
                    </Typography>
                )}
                <CardActions className = {classes.proses}>
                    <Link 
                        to ={{pathname: '/payment'}} 
                        className={classes.button}
                    >
                        Adopt
                    </Link>
                </CardActions>
            </CardContent>
        </Card>
    );
}

export default PlaceDetail;