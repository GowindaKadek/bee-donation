import React from 'react';
import { Autocomplete } from '@react-google-maps/api';
import { Link } from 'react-router-dom';
import {AppBar, Toolbar, Typography, InputBase, Box, Button} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import logo from '../../assets/image/logo.png';

import useStyle from './style';
import { useState } from 'react';
const Header = () => {
    const classes = useStyle();
    const [isLogin, setIsLogin] = useState(false);

    return(
        <AppBar position = "static">
            <Toolbar className = {classes.toolbar}>
                <Typography variant = "h5" className={classes.title}>
                    <img src = {logo} className = {classes.logo}>
                    </img>
                    Bee Adoption
                </Typography>
                <Box display = "flex" width="500px" justifyContent="space-between">
                    {/* <Autocomplete> */}
                        <div className={classes.search}>
                            <div className={classes.searchIcon}>
                                {/* <SearchIcon/> */}
                            </div>
                            <InputBase placeholder = "Search ..." classes={{root: classes.inputRoot, input: classes.inputInput}}/>
                        </div>
                    {/* </Autocomplete> */}

                    <div className={classes.account}>
                        {!isLogin &&
                            <Link to="/login" className={classes.button}>Login</Link>
                        }
                        {!isLogin &&
                            <Link to ="/register" className={classes.button}>Register</Link>
                        }
                    </div>
                </Box>
            </Toolbar>
        </AppBar>
    );
}

export default Header;