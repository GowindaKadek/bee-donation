import React, {useState} from 'react';
import { CircularProgress, Grid, Typography, InputLabel, MenuItem, FormControl, Select } from '@material-ui/core';

import PlaceDetail from '../PlaceDetail/PlaceDetail';
import useStyles from './style';
const List = ({places, childClicked}) => {
    const classes = useStyles();
    const [type,setType] = useState('Bee Keeper');
    const [rating, setRating] = useState('All');

    //console.log({childClicked});

    return(
        <div className={classes.container}>
            <Typography variant="h5" >Choose Your Adoption</Typography>
            <FormControl className={classes.formControl}>
              <InputLabel id="type">Type</InputLabel>
              <Select id="type" value={type} onChange={(e) => setType(e.target.value)} label ={type}>
                <MenuItem value="Bee-Keeper">Bee Kella</MenuItem>
                <MenuItem value="Bee-Farm">Kayu Putih</MenuItem>
                <MenuItem value="Product">Product</MenuItem>
              </Select>
            </FormControl>
          <FormControl className={classes.formControl}>
            <InputLabel id="rating">Rating</InputLabel>
            <Select id="rating" value={rating} onChange={(e) => setRating(e.target.value)}>
              <MenuItem value={0}>All</MenuItem>
              <MenuItem value={3}>Above 3.0</MenuItem>
              <MenuItem value={4}>Above 4.0</MenuItem>
              <MenuItem value={4.5}>Above 4.5</MenuItem>
            </Select>
          </FormControl>
          <Grid container spacing={3} className={classes.list}>
            {places?.map((place, i) => (
              <Grid item xs={12} key={i}>
                <PlaceDetail place={place} />
              </Grid>
            ))}
          </Grid>
        </div>
    );
}

export default List;