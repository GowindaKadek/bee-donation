import React from "react";
import './../assets/css/navbar.css';
import { useHistory, Link } from "react-router-dom";
import Logo from './../assets/image/logo.png';

export default function Navbar(){
    let history = useHistory();
    return(
        <div id = "component-navbar">
            <div className = "brand-container">
                <img src={Logo} className="brand-logo"/>
                <text>Bee Propolis</text>
            </div>
            <div className="link-navigation">
                <Link to = "/" >Home</Link>
            </div>
        </div>
    )
}