import React from "react";
import '../../assets/css/footer.css';
import Logo from '../../assets/image/logo.png';
import Facebook from '../../assets/image/facebook.png';
import Instagram from '../../assets/image/instagram.png';
import Whatsapp from '../../assets/image/whatsApp.png';
import Email from '../../assets/image/email.png';


export default function Footer() {

    return(
    <>
        <footer class="footer-distributed">
            <div class="footer-left">
                <h3>Adopsi<span>Bahan Alam</span> <img src={Logo} style={{width:'50px', height:'50px'}} /> </h3>
                <p class="footer-links">
                    <a href="#" class="link-1">Home</a>
                    <a href="#">Blog</a>              
                    <a href="#">Pricing</a>              
                    <a href="#">About</a>                   
                    <a href="#">Faq</a>                  
                    <a href="#">Contact</a>
                </p>
                <p class="footer-company-name">Bali Tresna Sujati © 20xx</p>
            </div>
            <div class="footer-center">
                <div>
                    <i class="fa fa-map-marker"></i>
                    <p>Banjarankan,, Jl. Raya Banjarangkan, Aan, Kec. Banjarangkan,<br/> Kabupaten Klungkung, Bali 80752, Indonesia</p>
                </div>

                <div>
                    <i class="fa fa-phone"></i>
                    <p>+6282 2374 xxxx</p>
                </div>

                <div>
                    <i class="fa fa-envelope"></i>
                    <p><a href="mailto:support@company.com">WebsiteAdopsi@gmail.com</a></p>
                </div>
            </div>
            <div class="footer-right">
                <p class="footer-company-about">
                    <span>About the Site</span>
                    Website Adopsi ini dikembangkan oleh yayasan Bali Trensa Sujati dalam rangka memberikan kesempatan pada mereka yang ingin berpartisipasi dalam pelesatrian alam.
                </p>
                <div class="footer-icons">
                    <a href="#"><i class="fa-facebook"></i> <img class = "img-icon" src = {Facebook}/> </a>
                    <a href="#"><i class="fa-instagram"></i> <img class = "img-icon" src = {Instagram}/> </a>
                    <a href="#"><i class="fa-whatsapp"></i> <img class = "img-icon" src = {Whatsapp}/> </a>
                    <a href="#"><i class="fa-email"></i> <img class = "img-icon" src = {Email}/> </a>
                </div>
            </div>
        </footer>
    </>
    );
}