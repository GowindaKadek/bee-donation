import { DataUsageOutlined } from "@material-ui/icons";
import dataa from "../Datajson";

export const getPlacesData = async () => {
    try {
        const data = dataa.map(function(item) {
            return {
              id : item.id,
              name: item.name,
              email : item.email,
              phone : item.phone,
              address : item.address,
              lat : item.lat,
              lng : item.lng,
              info : item.info,
              img : item.img
            };
          });

        return data;

    }catch (error){
        console.log(error);
    }
}
