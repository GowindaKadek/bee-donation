import React, { useState } from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import HomeScreen from './screens/HomeScreen';
import ProductScreen from './screens/ProductScreen';
import PaymentScreen from './screens/PaymentScreen';


const App = () => {
  
  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route path = '/' exact>{<HomeScreen/>}</Route>
          <Route path='/login' component={LoginScreen} exact/>
          <Route path='/register' component={RegisterScreen} exact/>
          <Route path='/products' component={ProductScreen} exact/>
          <Route path='/payment' component={PaymentScreen} exact/>
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
